declare interface INewReactWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'NewReactWebPartStrings' {
  const strings: INewReactWebPartStrings;
  export = strings;
}
