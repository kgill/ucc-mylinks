import * as React from "react";
import * as ReactDom from "react-dom";
import { Version } from "@microsoft/sp-core-library";
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from "@microsoft/sp-webpart-base";

import * as strings from "NewReactWebPartStrings";
import { NewReact, INewReactProps } from "./components/NewReactComponent";
import { WebPartContext } from "@microsoft/sp-webpart-base";
import * as jQuery from "jquery";
import { SPComponentLoader } from "@microsoft/sp-loader";

import {
  SPHttpClient,
  SPHttpClientResponse,
  SPHttpClientConfiguration
} from "@microsoft/sp-http";

export interface INewReactWebPartProps {
  description: string;
  context: WebPartContext;
  loginName: string;
  saveToUPFunction: any;
}

export default class NewReactWebPart extends BaseClientSideWebPart<
  INewReactWebPartProps
> {
  public render(): void {
    const loginName =
      "i:0#.f|membership|" + this.context.pageContext.user.loginName;

    const element: React.ReactElement<INewReactProps> = React.createElement(
      NewReact,
      {
        description: this.properties.description,
        context: this.context,
        userLoginName: loginName,
        saveToUPFunction: this.CommitChangesToUserProfile
      }
    );

    ReactDom.render(element, this.domElement);
  }
  public CommitChangesToUserProfile = data => {
    console.log("Data posted: " + data);
    var lName = "i:0#.f|membership|" + this.context.pageContext.user.loginName;

    let apiUrl =
      this.context.pageContext.web.absoluteUrl +
      "/_api/SP.UserProfiles.PeopleManager/SetSingleValueProfileProperty";
    let userData = {
      accountName: lName,
      propertyName: "UCCMyLinks", //can also be used to set custom single value profile properties
      propertyValue: data
    };
    let httpClient: SPHttpClient = this.context.spHttpClient;
    let spOpts = {
      headers: {
        Accept: "application/json;odata=nometadata",
        "Content-type": "application/json;odata=verbose",
        "odata-version": ""
      },
      body: JSON.stringify(userData)
    };
    httpClient
      .post(apiUrl, SPHttpClient.configurations.v1, spOpts)
      .then(response => {
        alert("Updated");
      });
  };

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse("1.0");
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField("description", {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
