import * as React from "react";
import styles from "./NewReact.module.scss";
import { INewReactProps } from "./INewReactProps";
import { escape } from "@microsoft/sp-lodash-subset";
import { ReactForm } from "../ReactForm";

export class NewReact extends React.Component<INewReactProps, {}> {
  public render(): React.ReactElement<INewReactProps> {
    return (
      <div className={styles.newReact}>
        <ReactForm
          context={this.props.context}
          userLoginName={this.props.userLoginName}
          saveToUPFunction={this.props.saveToUPFunction}
        />
      </div>
    );
  }
}
