export interface IReactFormState {
  title: string;
  url: string;
  rowReference: Number;
  urlLinks: any[];
  submitButtonState: string;
}
