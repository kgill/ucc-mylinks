import * as React from "react";
import { IReactFormProps } from "./IReactFormProps";
import { IReactFormState } from "./IReactFormState";
import { Row } from "../Row";
import * as jQuery from "jquery";
import arrayMove from "array-move";
import styles from "../NewReactComponent/NewReact.module.scss";
import { Button } from "office-ui-fabric-react/lib/Button";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { Label } from "office-ui-fabric-react/lib/Label";

export class ReactForm extends React.Component<
  IReactFormProps,
  IReactFormState
> {
  componentWillMount() {
    var newArray;
    jQuery.ajax({
      url: "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",
      headers: { Accept: "application/json;odata=verbose" },
      success: function(data) {
        try {
          var properties = data.d.UserProfileProperties.results;
          for (var i = 0; i < properties.length; i++) {
            var property = properties[i];

            if (property.Key == "UCCMyLinks") {
              var links = property.Value;
              newArray = JSON.parse(links);
            }
          }
          if (newArray) {
            this.setState({ urlLinks: newArray });
          }
        } catch (err2) {
          console.log(JSON.stringify(err2));
        }
      }.bind(this), //in order to make SetState available inside the success function
      error: function(jQxhr, errorCode, errorThrown) {
        console.log(errorThrown);
      }
    });
  }

  constructor() {
    super();
    this.state = {
      title: "",
      url: "",
      rowReference: null,
      urlLinks: [],
      submitButtonState: "Add"
    };
    this._onChange = this._onChange.bind(this);
  }
  public render(): React.ReactElement<IReactFormProps> {
    return (
      <div className="UCC-Rollup">
        <div className="card ms-Grid-row">
          <div className="card-header">
            <h3>Edit My Quick Links</h3>
          </div>

          <div className="card-body">
            <form>
              <Label className={styles.label}>Enter Link Title</Label>
              <TextField
                required={true}
                placeholder="Title"
                id="CustomLinkTitle"
                value={this.state.title}
                onChanged={value => this._onChange("title", value)}
              />
              <Label className={styles.label}>Enter URL</Label>
              <TextField
                required={true}
                placeholder="URL"
                id="CustomLinkUrl"
                value={this.state.url}
                onChanged={value => this._onChange("url", value)}
              />
              <Button
                name="input"
                type="submit"
                className={styles.uccButton}
                id="CustomSubmitButton"
                onClick={e => this.onSubmit(e)}
              >
                {this.state.submitButtonState}
              </Button>

              <Button
                name="input"
                type="submit"
                id="CustomSubmitButton"
                className={styles.uccButton}
                onClick={e => this.commitToUserProfile(e)}
              >
                Save All Links
              </Button>
              <input type="hidden" className="row-ref" value="" />
            </form>
            <hr />
            <table className={styles.urlLinks}>
              <thead>
                <tr>
                  <th>Link Title</th>
                  <th>Link URL</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                {this.state.urlLinks.map((data, index) => {
                  return (
                    <Row
                      data={data}
                      key={index}
                      row={index}
                      editRow={this.editRow.bind(this)}
                      deleteRow={this.deleteRow.bind(this)}
                      moveUp={this.moveUp.bind(this)}
                      moveDown={this.moveDown.bind(this)}
                    />
                  );
                })}
              </tbody>
            </table>
          </div>
          <div id="myLinksErrorMessage" className={styles.errorMessage} />
        </div>
      </div>
    );
  }

  private _onChange(sourceState, value) {
    var obj = {};
    obj[sourceState] = value;
    return this.setState(obj);
  }
  public moveUp(row) {
    if (row != 0) {
      var newArray = arrayMove(this.state.urlLinks, row, row - 1);
      this.setState({ urlLinks: newArray });
    }
  }
  public moveDown(row) {
    if (row != this.state.urlLinks.length) {
      var newArray = arrayMove(this.state.urlLinks, row, row + 1);
      this.setState({ urlLinks: newArray });
    }
  }
  public commitToUserProfile(e) {
    e.preventDefault();
    var currentState = JSON.stringify(this.state.urlLinks);
    this.props.saveToUPFunction(currentState);
  }
  public onSubmit(e) {
    e.preventDefault();
    var title = (document.getElementById("CustomLinkTitle") as HTMLInputElement)
      .value;
    var url = (document.getElementById("CustomLinkUrl") as HTMLInputElement)
      .value;

    if (this.state.submitButtonState == "Add") {
      if (title === "" || url == "") {
        jQuery("#myLinksErrorMessage").html(
          "Title and URL are mandatory fields"
        );
        return;
      }
      let obj = { title: this.state.title, url: this.state.url };

      this.setState(
        {
          urlLinks: [...this.state.urlLinks, obj]
        },
        () => console.log(this.state.urlLinks)
      );
    }
    if (this.state.submitButtonState == "Edit") {
      let newArray = this.state.urlLinks;

      const pos = Number(this.state.rowReference);
      this.state.urlLinks[pos].title = title;
      this.state.urlLinks[pos].url = url;
      this.clearInputs();
      this.forceUpdate();

      this.setState({
        submitButtonState: "Add"
      });
    }
  }
  public deleteRow(row) {
    var newArray = this.state.urlLinks;
    newArray.splice(row, 1);
    this.setState({ urlLinks: newArray });
    this.clearInputs();
  }
  public editRow(row) {
    this.setState({
      title: this.state.urlLinks[row].title,
      url: this.state.urlLinks[row].url,
      rowReference: row,
      submitButtonState: "Edit"
    });
  }
  public clearInputs() {
    (document.getElementById("CustomLinkTitle") as HTMLInputElement).value = "";
    (document.getElementById("CustomLinkUrl") as HTMLInputElement).value = "";
    this.setState({
      title: "",
      url: "",
      rowReference: -1
    });
  }
}
