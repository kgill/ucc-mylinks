import * as React from "react";
import { IRowProps } from "./IRowProps";
import styles from "../NewReactComponent/NewReact.module.scss";

export class Row extends React.Component<IRowProps, {}> {
  constructor() {
    super();
  }
  public render(): React.ReactElement<IRowProps> {
    const deleteIconStyle = "ms-Icon ms-Icon--Delete " + styles.mediumSize;
    const editIconStyle =
      "ms-Icon ms-Icon--SingleColumnEdit " + styles.mediumSize;
    const moveUpStyle = "ms-Icon ms-Icon--ChevronUpSmall " + styles.mediumSize;
    const moveDownStyle =
      "ms-Icon ms-Icon--ChevronDownSmall " + styles.mediumSize;

    return (
      <tr className="ms-Table-row">
        <td>{this.props.data.title}</td>
        <td className={styles.content}>{this.props.data.url}</td>
        <td className={styles.action}>
          <i
            className={deleteIconStyle}
            title="Delete"
            aria-hidden="true"
            onClick={() => {
              this.props.deleteRow(this.props.row);
            }}
          />
          <i
            className={editIconStyle}
            title="Edit"
            aria-hidden="true"
            onClick={() => {
              this.props.editRow(this.props.row);
            }}
          />
          <i
            className={moveUpStyle}
            title="Move up"
            aria-hidden="true"
            onClick={() => {
              this.props.moveUp(this.props.row);
            }}
          />
          <i
            className={moveDownStyle}
            title="Move down"
            aria-hidden="true"
            onClick={() => {
              this.props.moveDown(this.props.row);
            }}
          />
        </td>
      </tr>
    );
  }
}
