export interface IRowProps {
  description?: string;
  deleteRow: any;
  editRow: any;
  row: Number;
  data: { title: string; url: string };
  moveUp: Function;
  moveDown: Function;
}
